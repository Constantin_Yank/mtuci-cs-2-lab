# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()
secret_word = choose_word(wordlist)


def is_word_guessed(secret_word, letters_guessed):
    letters_guessed = ''.join(letters_guessed)
    count = 0
    for letter in secret_word:
        if letter in letters_guessed:
            count += 1
    if count == len(list(secret_word)):
        return True
    else: return False      


def get_guessed_word(secret_word, letters_guessed):
    global count_try
    
    if letters_guessed[-1] in secret_word:
        print('Good guess:')
    else:
        print('Oops! That letter is not in my word:')
        
    for letter in secret_word:
        if letter in letters_guessed:
            print(letter, end = ' ')
        else:
            print('_',end= ' ')
    
            
    if letters_guessed[-1] not in secret_word and letters_guessed[-1] not in 'aeiouy':
        count_try -= 1
    elif (letters_guessed[-1] not in secret_word) and (letters_guessed[-1] in 'aeiouy'):
        count_try -= 2



letters = 'abcdefghijklmnopqrstuvwxyz'

def get_available_letters(letters_guessed):
    global letters, warning
    warning = 3
    #try:
    enter_letter = str(input('Please guess a letter: '))
    enter_letter = enter_letter.lower()
    #except TypeError: 
        #print(f'You can enter only one letter of the Latin alphabet! You have {warning - 1} warnings left.')
        #warning -= 1
    letters_guessed.append(enter_letter)
    
        
    for i in list(letters):
        if i in letters_guessed:
            letters = letters.replace(i,'')
    return letters_guessed

letters_guessed = []


def hangman(secret_word):
    print('Welcome to the game Hungman')
    print(f'I am thinking of a word that is {len(secret_word)} letters long.')
    print('-------------------------------------------')
    
    global count_try
    count_try = 7
    while count_try > 1:
        print('You have 3 warnings left.')
        print(f'You have {count_try - 1} guesses left.')
        print(f'Available letters: {letters}')

        get_available_letters(letters_guessed)    
        get_guessed_word(secret_word,letters_guessed)    
        if is_word_guessed(secret_word,letters_guessed):
            return True
        print('\n\n''.................................')
    return False 
        
    
        
        
    

if hangman(secret_word):
    print('\n''..)/(/(/(')
    print('...(◉◡◉)')
    print('(|)►II◄(|)')
    print('..[ll♥II]')
    print('.(,,).(,,)')
    print('С товими мозгами тебе только в шахматы играть')
else:
    print('._____,==.______________|~~~')
    print('.____/__00\_____________| [5]')
    print('.____\c__-_)_________|~~~')
    print('._____`)_(___________| [4]')
    print('._____/---\_______|~~~')
    print('.____/___\_\______| [3]')
    print('.___((___/\_\_ |~~~')
    print('.____\\_-\_`--`| [2]')
    print('.____/_/_/__|~~~')
    print('.__ (_(___)_| [1]')
    print('Ты не виноват, просто код недоработанный')
    print(f'А слово это, {secret_word}')
    



# ----------------------------------





    



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    
    secret_word = choose_word(wordlist)
    hangman(secret_word)

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    #secret_word = choose_word(wordlist)
    #hangman_with_hints(secret_word)

